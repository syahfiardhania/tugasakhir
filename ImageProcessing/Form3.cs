﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageProcessing
{
    public partial class Form3 : Form
    {
        Rectangle rect;

        Point LocationXY;
        Point LocationX1Y1;

        bool IsMouseDown = false;

        public Form3()
        {
            InitializeComponent();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void btnOpenImg_Click(object sender, EventArgs e)
        {
            OpenFileDialog openImgFile = new OpenFileDialog();

            openImgFile.Filter = "Image Files (*.bmp,*.gif,*.jpg)|*.bmp;*.gif;*.jpg";

            if(openImgFile.ShowDialog() == DialogResult.OK)
            {
                picFromImg.Image = Image.FromFile(openImgFile.FileName, true);
            }
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (pictureBox3.Image != null)
            {
                SaveFileDialog saveImg = new SaveFileDialog();
                saveImg.Filter = "Image Files (*.bmp,*.gif,*.jpg)|*.bmp;*.gif;*.jpg";

                if (saveImg.ShowDialog() == DialogResult.OK)
                {
                    /*if (saveImg.FileName.EndsWith(".bmp"))
                    {
                        pictureBox3.Image.Save(saveImg.FileName, );
                    }*/
                }
            }
            else
            {
                MessageBox.Show("No Image to Save ..");
            }
        }

        private void picFromImg_Click(object sender, EventArgs e)
        {

        }

        private void picFromImg_MouseDown(object sender, MouseEventArgs e)
        {
            IsMouseDown = true;
            LocationXY = e.Location;
        }

        private void picFromImg_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsMouseDown == true)
            {
                LocationX1Y1 = e.Location;
                Refresh();
            }
        }

        private void picFromImg_MouseUp(object sender, MouseEventArgs e)
        {
            if (IsMouseDown == true)
            {
                LocationX1Y1 = e.Location;

                IsMouseDown = false;

                if (rect != null)
                {
                    Bitmap bit = new Bitmap(picFromImg.Image, picFromImg.Width, picFromImg.Height);

                    Bitmap cropImg = new Bitmap(rect.Width, rect.Height);

                    Graphics g = Graphics.FromImage(cropImg);
                    g.DrawImage(bit, 0, 0, rect, GraphicsUnit.Pixel);
                    pictureBox3.Image = cropImg;
                }
            }
        }

        private void picFromImg_Paint(object sender, PaintEventArgs e)
        {
            if(rect != null)
            {
                e.Graphics.DrawRectangle(Pens.Blue, Getrect());
            }
        }

        private Rectangle Getrect()
        {
            rect = new Rectangle();

            rect.X = Math.Min(LocationXY.X, LocationX1Y1.X);

            rect.Y = Math.Min(LocationXY.Y, LocationX1Y1.Y);

            rect.Width = Math.Abs(LocationXY.X - LocationX1Y1.X);

            rect.Height = Math.Abs(LocationXY.Y - LocationX1Y1.Y);

            return rect;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 form;

            form = new Form1();
            form.Show();
        }

        private void btn_croping_Click(object sender, EventArgs e)
        {

        }
    }
}
