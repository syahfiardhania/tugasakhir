﻿namespace ImageProcessing
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.picFromImg = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnOpenImg = new System.Windows.Forms.Button();
            this.btn_croping = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picFromImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // picFromImg
            // 
            this.picFromImg.Image = ((System.Drawing.Image)(resources.GetObject("picFromImg.Image")));
            this.picFromImg.Location = new System.Drawing.Point(1500, 263);
            this.picFromImg.Name = "picFromImg";
            this.picFromImg.Size = new System.Drawing.Size(307, 243);
            this.picFromImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFromImg.TabIndex = 0;
            this.picFromImg.TabStop = false;
            this.picFromImg.Click += new System.EventHandler(this.picFromImg_Click);
            this.picFromImg.Paint += new System.Windows.Forms.PaintEventHandler(this.picFromImg_Paint);
            this.picFromImg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picFromImg_MouseDown);
            this.picFromImg.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picFromImg_MouseMove);
            this.picFromImg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picFromImg_MouseUp);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(12, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1280, 960);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // btnOpenImg
            // 
            this.btnOpenImg.Location = new System.Drawing.Point(1575, 184);
            this.btnOpenImg.Name = "btnOpenImg";
            this.btnOpenImg.Size = new System.Drawing.Size(191, 42);
            this.btnOpenImg.TabIndex = 3;
            this.btnOpenImg.Text = "Load";
            this.btnOpenImg.UseVisualStyleBackColor = true;
            this.btnOpenImg.Click += new System.EventHandler(this.btnOpenImg_Click);
            // 
            // btn_croping
            // 
            this.btn_croping.Location = new System.Drawing.Point(1699, 599);
            this.btn_croping.Name = "btn_croping";
            this.btn_croping.Size = new System.Drawing.Size(75, 23);
            this.btn_croping.TabIndex = 4;
            this.btn_croping.Text = "button1";
            this.btn_croping.UseVisualStyleBackColor = true;
            this.btn_croping.Click += new System.EventHandler(this.btn_croping_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 1055);
            this.Controls.Add(this.btn_croping);
            this.Controls.Add(this.btnOpenImg);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFromImg);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form3";
            ((System.ComponentModel.ISupportInitialize)(this.picFromImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFromImg;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnOpenImg;
        private System.Windows.Forms.Button btn_croping;
    }
}