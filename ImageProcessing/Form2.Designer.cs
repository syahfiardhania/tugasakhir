﻿namespace ImageProcessing
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_browse = new System.Windows.Forms.Button();
            this.btn_roi = new System.Windows.Forms.Button();
            this.pb_roi = new System.Windows.Forms.PictureBox();
            this.pb_sobel = new System.Windows.Forms.PictureBox();
            this.pb_crop_iris = new System.Windows.Forms.PictureBox();
            this.btn_sobel = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pb_roi_img = new System.Windows.Forms.PictureBox();
            this.tx_hasil = new System.Windows.Forms.Label();
            this.btn_kondisi = new System.Windows.Forms.Button();
            this.txt_ratio_bw = new System.Windows.Forms.Label();
            this.pb_extract = new System.Windows.Forms.PictureBox();
            this.txt_bw = new System.Windows.Forms.Label();
            this.btn_rasio = new System.Windows.Forms.Button();
            this.edit_threshold = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.pb_roi2 = new System.Windows.Forms.PictureBox();
            this.pb_roi_img2 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pb_extract2 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_ratio_bw2 = new System.Windows.Forms.Label();
            this.txt_bw2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tx_hasil2 = new System.Windows.Forms.Label();
            this.edit_threshold2 = new System.Windows.Forms.TextBox();
            this.btn_kondisi2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_sobel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_crop_iris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_extract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_img2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_extract2)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(140, 303);
            this.btn_browse.Margin = new System.Windows.Forms.Padding(4);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(100, 41);
            this.btn_browse.TabIndex = 71;
            this.btn_browse.Text = "Load Iris\r\n";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // btn_roi
            // 
            this.btn_roi.Location = new System.Drawing.Point(1088, 328);
            this.btn_roi.Margin = new System.Windows.Forms.Padding(4);
            this.btn_roi.Name = "btn_roi";
            this.btn_roi.Size = new System.Drawing.Size(100, 39);
            this.btn_roi.TabIndex = 70;
            this.btn_roi.Text = "ROI Area";
            this.btn_roi.UseVisualStyleBackColor = true;
            this.btn_roi.Click += new System.EventHandler(this.btn_roi_Click);
            // 
            // pb_roi
            // 
            this.pb_roi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_roi.Location = new System.Drawing.Point(825, 16);
            this.pb_roi.Margin = new System.Windows.Forms.Padding(4);
            this.pb_roi.Name = "pb_roi";
            this.pb_roi.Size = new System.Drawing.Size(293, 270);
            this.pb_roi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_roi.TabIndex = 69;
            this.pb_roi.TabStop = false;
            // 
            // pb_sobel
            // 
            this.pb_sobel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_sobel.Location = new System.Drawing.Point(443, 15);
            this.pb_sobel.Margin = new System.Windows.Forms.Padding(4);
            this.pb_sobel.Name = "pb_sobel";
            this.pb_sobel.Size = new System.Drawing.Size(293, 270);
            this.pb_sobel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_sobel.TabIndex = 68;
            this.pb_sobel.TabStop = false;
            // 
            // pb_crop_iris
            // 
            this.pb_crop_iris.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_crop_iris.Location = new System.Drawing.Point(60, 25);
            this.pb_crop_iris.Margin = new System.Windows.Forms.Padding(4);
            this.pb_crop_iris.Name = "pb_crop_iris";
            this.pb_crop_iris.Size = new System.Drawing.Size(293, 270);
            this.pb_crop_iris.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_crop_iris.TabIndex = 67;
            this.pb_crop_iris.TabStop = false;
            // 
            // btn_sobel
            // 
            this.btn_sobel.Location = new System.Drawing.Point(537, 293);
            this.btn_sobel.Margin = new System.Windows.Forms.Padding(4);
            this.btn_sobel.Name = "btn_sobel";
            this.btn_sobel.Size = new System.Drawing.Size(100, 41);
            this.btn_sobel.TabIndex = 66;
            this.btn_sobel.Text = "Sobel";
            this.btn_sobel.UseVisualStyleBackColor = true;
            this.btn_sobel.Click += new System.EventHandler(this.btn_sobel_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pb_roi_img
            // 
            this.pb_roi_img.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_roi_img.Location = new System.Drawing.Point(60, 436);
            this.pb_roi_img.Margin = new System.Windows.Forms.Padding(4);
            this.pb_roi_img.Name = "pb_roi_img";
            this.pb_roi_img.Size = new System.Drawing.Size(224, 128);
            this.pb_roi_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_roi_img.TabIndex = 72;
            this.pb_roi_img.TabStop = false;
            // 
            // tx_hasil
            // 
            this.tx_hasil.AutoSize = true;
            this.tx_hasil.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tx_hasil.ForeColor = System.Drawing.Color.Red;
            this.tx_hasil.Location = new System.Drawing.Point(1464, 500);
            this.tx_hasil.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tx_hasil.Name = "tx_hasil";
            this.tx_hasil.Size = new System.Drawing.Size(73, 24);
            this.tx_hasil.TabIndex = 80;
            this.tx_hasil.Text = "HASIL: ";
            // 
            // btn_kondisi
            // 
            this.btn_kondisi.Location = new System.Drawing.Point(1115, 519);
            this.btn_kondisi.Margin = new System.Windows.Forms.Padding(4);
            this.btn_kondisi.Name = "btn_kondisi";
            this.btn_kondisi.Size = new System.Drawing.Size(172, 41);
            this.btn_kondisi.TabIndex = 79;
            this.btn_kondisi.Text = "Check Kondisi";
            this.btn_kondisi.UseVisualStyleBackColor = true;
            this.btn_kondisi.Click += new System.EventHandler(this.btn_kondisi_Click);
            // 
            // txt_ratio_bw
            // 
            this.txt_ratio_bw.AutoSize = true;
            this.txt_ratio_bw.Location = new System.Drawing.Point(643, 691);
            this.txt_ratio_bw.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txt_ratio_bw.Name = "txt_ratio_bw";
            this.txt_ratio_bw.Size = new System.Drawing.Size(20, 17);
            this.txt_ratio_bw.TabIndex = 78;
            this.txt_ratio_bw.Text = "...";
            // 
            // pb_extract
            // 
            this.pb_extract.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_extract.Location = new System.Drawing.Point(581, 436);
            this.pb_extract.Margin = new System.Windows.Forms.Padding(4);
            this.pb_extract.Name = "pb_extract";
            this.pb_extract.Size = new System.Drawing.Size(224, 128);
            this.pb_extract.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_extract.TabIndex = 77;
            this.pb_extract.TabStop = false;
            this.pb_extract.Click += new System.EventHandler(this.pb_extract_Click);
            // 
            // txt_bw
            // 
            this.txt_bw.AutoSize = true;
            this.txt_bw.Location = new System.Drawing.Point(1050, 747);
            this.txt_bw.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txt_bw.Name = "txt_bw";
            this.txt_bw.Size = new System.Drawing.Size(20, 17);
            this.txt_bw.TabIndex = 76;
            this.txt_bw.Text = "...";
            // 
            // btn_rasio
            // 
            this.btn_rasio.Location = new System.Drawing.Point(221, 692);
            this.btn_rasio.Margin = new System.Windows.Forms.Padding(4);
            this.btn_rasio.Name = "btn_rasio";
            this.btn_rasio.Size = new System.Drawing.Size(147, 39);
            this.btn_rasio.TabIndex = 81;
            this.btn_rasio.Text = "Ekstraksi Fitur";
            this.btn_rasio.UseVisualStyleBackColor = true;
            this.btn_rasio.Click += new System.EventHandler(this.btn_rasio_Click);
            // 
            // edit_threshold
            // 
            this.edit_threshold.Location = new System.Drawing.Point(1116, 489);
            this.edit_threshold.Margin = new System.Windows.Forms.Padding(4);
            this.edit_threshold.Name = "edit_threshold";
            this.edit_threshold.Size = new System.Drawing.Size(171, 22);
            this.edit_threshold.TabIndex = 82;
            this.edit_threshold.Text = "0,266";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Tan;
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(1123, 394);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 17);
            this.label1.TabIndex = 83;
            this.label1.Text = "threshold rasio putih";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.SaddleBrown;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(778, 394);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.TabIndex = 86;
            this.label2.Text = "Distance";
            // 
            // pb_roi2
            // 
            this.pb_roi2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_roi2.Location = new System.Drawing.Point(1146, 16);
            this.pb_roi2.Margin = new System.Windows.Forms.Padding(4);
            this.pb_roi2.Name = "pb_roi2";
            this.pb_roi2.Size = new System.Drawing.Size(293, 270);
            this.pb_roi2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_roi2.TabIndex = 87;
            this.pb_roi2.TabStop = false;
            // 
            // pb_roi_img2
            // 
            this.pb_roi_img2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_roi_img2.Location = new System.Drawing.Point(336, 436);
            this.pb_roi_img2.Margin = new System.Windows.Forms.Padding(4);
            this.pb_roi_img2.Name = "pb_roi_img2";
            this.pb_roi_img2.Size = new System.Drawing.Size(162, 192);
            this.pb_roi_img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_roi_img2.TabIndex = 89;
            this.pb_roi_img2.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(924, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 17);
            this.label3.TabIndex = 91;
            this.label3.Text = "Penyakit Diabetes";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1244, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 17);
            this.label4.TabIndex = 92;
            this.label4.Text = "Penyakit Jantung";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(134, 642);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 17);
            this.label5.TabIndex = 93;
            this.label5.Text = "Penyakit Diabetes";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(333, 642);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 17);
            this.label6.TabIndex = 94;
            this.label6.Text = "Penyakit Jantung";
            // 
            // pb_extract2
            // 
            this.pb_extract2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_extract2.Location = new System.Drawing.Point(850, 434);
            this.pb_extract2.Margin = new System.Windows.Forms.Padding(4);
            this.pb_extract2.Name = "pb_extract2";
            this.pb_extract2.Size = new System.Drawing.Size(162, 192);
            this.pb_extract2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_extract2.TabIndex = 95;
            this.pb_extract2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(643, 642);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 17);
            this.label7.TabIndex = 96;
            this.label7.Text = "Penyakit Diabetes";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(871, 642);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 17);
            this.label8.TabIndex = 97;
            this.label8.Text = "Penyakit Jantung";
            // 
            // txt_ratio_bw2
            // 
            this.txt_ratio_bw2.AutoSize = true;
            this.txt_ratio_bw2.Location = new System.Drawing.Point(862, 691);
            this.txt_ratio_bw2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txt_ratio_bw2.Name = "txt_ratio_bw2";
            this.txt_ratio_bw2.Size = new System.Drawing.Size(20, 17);
            this.txt_ratio_bw2.TabIndex = 98;
            this.txt_ratio_bw2.Text = "...";
            // 
            // txt_bw2
            // 
            this.txt_bw2.AutoSize = true;
            this.txt_bw2.Location = new System.Drawing.Point(1259, 747);
            this.txt_bw2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txt_bw2.Name = "txt_bw2";
            this.txt_bw2.Size = new System.Drawing.Size(20, 17);
            this.txt_bw2.TabIndex = 99;
            this.txt_bw2.Text = "...";
            this.txt_bw2.Click += new System.EventHandler(this.txt_bw2_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1050, 697);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 17);
            this.label9.TabIndex = 100;
            this.label9.Text = "Penyakit Diabetes";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1259, 697);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 17);
            this.label10.TabIndex = 101;
            this.label10.Text = "Penyakit Jantung";
            // 
            // tx_hasil2
            // 
            this.tx_hasil2.AutoSize = true;
            this.tx_hasil2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tx_hasil2.ForeColor = System.Drawing.Color.Yellow;
            this.tx_hasil2.Location = new System.Drawing.Point(1464, 561);
            this.tx_hasil2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tx_hasil2.Name = "tx_hasil2";
            this.tx_hasil2.Size = new System.Drawing.Size(73, 24);
            this.tx_hasil2.TabIndex = 102;
            this.tx_hasil2.Text = "HASIL: ";
            // 
            // edit_threshold2
            // 
            this.edit_threshold2.Location = new System.Drawing.Point(1116, 597);
            this.edit_threshold2.Margin = new System.Windows.Forms.Padding(4);
            this.edit_threshold2.Name = "edit_threshold2";
            this.edit_threshold2.Size = new System.Drawing.Size(171, 22);
            this.edit_threshold2.TabIndex = 103;
            this.edit_threshold2.Text = "0,256";
            // 
            // btn_kondisi2
            // 
            this.btn_kondisi2.Location = new System.Drawing.Point(1116, 629);
            this.btn_kondisi2.Margin = new System.Windows.Forms.Padding(4);
            this.btn_kondisi2.Name = "btn_kondisi2";
            this.btn_kondisi2.Size = new System.Drawing.Size(172, 41);
            this.btn_kondisi2.TabIndex = 104;
            this.btn_kondisi2.Text = "Check Kondisi";
            this.btn_kondisi2.UseVisualStyleBackColor = true;
            this.btn_kondisi2.Click += new System.EventHandler(this.btn_kondisi2_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1148, 576);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 17);
            this.label11.TabIndex = 105;
            this.label11.Text = "Penyakit Jantung";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1138, 468);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(122, 17);
            this.label12.TabIndex = 106;
            this.label12.Text = "Penyakit Diabetes";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkKhaki;
            this.ClientSize = new System.Drawing.Size(1774, 923);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btn_kondisi2);
            this.Controls.Add(this.edit_threshold2);
            this.Controls.Add(this.tx_hasil2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_bw2);
            this.Controls.Add(this.txt_ratio_bw2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pb_extract2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pb_roi_img2);
            this.Controls.Add(this.pb_roi2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edit_threshold);
            this.Controls.Add(this.btn_rasio);
            this.Controls.Add(this.tx_hasil);
            this.Controls.Add(this.btn_kondisi);
            this.Controls.Add(this.txt_ratio_bw);
            this.Controls.Add(this.pb_extract);
            this.Controls.Add(this.txt_bw);
            this.Controls.Add(this.pb_roi_img);
            this.Controls.Add(this.btn_browse);
            this.Controls.Add(this.btn_roi);
            this.Controls.Add(this.pb_roi);
            this.Controls.Add(this.pb_sobel);
            this.Controls.Add(this.pb_crop_iris);
            this.Controls.Add(this.btn_sobel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_sobel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_crop_iris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_extract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_img2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_extract2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.Button btn_roi;
        private System.Windows.Forms.PictureBox pb_roi;
        private System.Windows.Forms.PictureBox pb_sobel;
        private System.Windows.Forms.PictureBox pb_crop_iris;
        private System.Windows.Forms.Button btn_sobel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pb_roi_img;
        private System.Windows.Forms.Label tx_hasil;
        private System.Windows.Forms.Button btn_kondisi;
        private System.Windows.Forms.Label txt_ratio_bw;
        private System.Windows.Forms.PictureBox pb_extract;
        private System.Windows.Forms.Label txt_bw;
        private System.Windows.Forms.Button btn_rasio;
        private System.Windows.Forms.TextBox edit_threshold;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pb_roi2;
        private System.Windows.Forms.PictureBox pb_roi_img2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pb_extract2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label txt_ratio_bw2;
        private System.Windows.Forms.Label txt_bw2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label tx_hasil2;
        private System.Windows.Forms.TextBox edit_threshold2;
        private System.Windows.Forms.Button btn_kondisi2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}