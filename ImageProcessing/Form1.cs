﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ImageProcessing
{
    public partial class Form1 : Form
    {
        Bitmap bmp_base, bmp_crop;
        Bitmap bmp_biner, bmp_crop_biner, bmp_crop_color, bmp_crop_color2;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_load_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                bmp_base = new Bitmap(openFileDialog1.FileName);
                pictureBox_base.Image = bmp_base;
                int counts = 1;
                for (int y = 0; y < 3; y++)
                {
                    for (int x = 0; x < 3; x++)
                    {
                        Color w1 = bmp_base.GetPixel(x, y);
                        Console.WriteLine(counts + "-" + w1.R + "-" + w1.G + "-" + w1.B);
                        counts++;
                    }
                }
            }
        }

        private void btn_crop_Click(object sender, EventArgs e)
        {
            /*int left = bmp_base.Width / 3;
        int right = bmp_base.Width / 3 * 2;
        int top = bmp_base.Height / 3;
        int bottom = bmp_base.Height / 3 * 2;*/

            /* int left = bmp_base.Width / 49 * 19;
             int right = bmp_base.Width / 49 * 33;
             int top = bmp_base.Height / 66 * 29 ;
             int bottom = bmp_base.Height / 66 * 39; 
             */
            //yang baru
            /*
            int left = bmp_base.Width / 100 * 10 ;
            int right = bmp_base.Width / 100 * 88;
            int top = bmp_base.Height / 100 * 2 ;
            int bottom = bmp_base.Height / 100 * 90;
            */
            int left = bmp_base.Width / 100 * 5;
            int right = bmp_base.Width / 100 * 93;
            int top = bmp_base.Height / 100 * 2;
            int bottom = bmp_base.Height / 100 * 95;

            int crop_width = right - left;
            int crop_height = bottom - top;
            bmp_crop = new Bitmap(crop_width, crop_height);

            int count = 0;

            int xc = 0, yc = 0;
            for (int x = left + 1; x <= right; x++)
            {
                yc = 0;
                for (int y = top + 1; y <= bottom; y++)
                {
                    Color w = bmp_base.GetPixel(x, y);
                    bmp_crop.SetPixel(xc, yc, w);
                    yc++;
                    count++;
                }
                xc++;
            }


            ////

            pictureBox_crop.Image = bmp_crop;
            labeltext.Text = "cropped";
        }

        private void btn_medianfilter_Click(object sender, EventArgs e)
        {
            Bitmap bmp_medianfilter = new Bitmap(bmp_crop);
            Color[] w = new Color[9];
            int[] wr = new int[9];
            int[] wg = new int[9];
            int[] wb = new int[9];
            for (int x = 0; x < bmp_crop.Width; x++)
            {
                for (int y = 0; y < bmp_crop.Height; y++)
                {
                    if (x - 1 < 0 || y - 1 < 0)
                        w[0] = Color.FromArgb(0, 0, 0);
                    else
                        w[0] = bmp_crop.GetPixel(x - 1, y - 1);

                    if (y - 1 < 0)
                        w[1] = Color.FromArgb(0, 0, 0);
                    else
                        w[1] = bmp_crop.GetPixel(x, y - 1);

                    if (x + 1 > bmp_crop.Width - 1 || y - 1 < 0)
                        w[2] = Color.FromArgb(0, 0, 0);
                    else
                        w[2] = bmp_crop.GetPixel(x + 1, y - 1);

                    if (x - 1 < 0)
                        w[3] = Color.FromArgb(0, 0, 0);
                    else
                        w[3] = bmp_crop.GetPixel(x - 1, y);

                    w[4] = bmp_crop.GetPixel(x, y);

                    if (x + 1 > bmp_crop.Width - 1)
                        w[5] = Color.FromArgb(0, 0, 0);
                    else
                        w[5] = bmp_crop.GetPixel(x + 1, y);

                    if (x - 1 < 0 || y + 1 > bmp_crop.Height - 1)
                        w[6] = Color.FromArgb(0, 0, 0);
                    else
                        w[6] = bmp_crop.GetPixel(x - 1, y + 1);

                    if (y + 1 > bmp_crop.Height - 1)
                        w[7] = Color.FromArgb(0, 0, 0);
                    else
                        w[7] = bmp_crop.GetPixel(x, y + 1);

                    if (x + 1 > bmp_crop.Width - 1 || y + 1 > bmp_crop.Height - 1)
                        w[8] = Color.FromArgb(0, 0, 0);
                    else
                        w[8] = bmp_crop.GetPixel(x + 1, y + 1);


                    for (int j = 0; j < w.Length; j++)
                    {
                        wr[j] = w[j].R;
                        wg[j] = w[j].G;
                        wb[j] = w[j].B;
                    }

                    Array.Sort(wr);
                    Array.Sort(wg);
                    Array.Sort(wb);

                    Color new_w = Color.FromArgb(wr[4], wg[4], wb[4]);
                    bmp_medianfilter.SetPixel(x, y, new_w);

                }
            }


            bmp_crop = bmp_medianfilter;

            pictureBox_crop.Image = bmp_crop;
            labeltext.Text = "filtered";
        }

        private void btn_sharp_Click(object sender, EventArgs e)
        {
            Bitmap bmp_sharpen = new Bitmap(bmp_crop);
            int[] filter_sharpen = { 0, -1, 0, -1, 5, -1, 0, -1, 0 };
            //{1,1,1,1,4,1,1,1,1};
            //jadi gelap int[] filter_sharpen = { -1, 0, 1, -1, 0, 3, -3, 0, 1 };


            for (int x = 0; x < bmp_crop.Width; x++)
            {
                for (int y = 0; y < bmp_crop.Height; y++)
                {
                    Color[] w = new Color[9];
                    int[] wr = new int[9];
                    int[] wg = new int[9];
                    int[] wb = new int[9];
                    int xr = 0, xg = 0, xb = 0;
                    if (x - 1 < 0 || y - 1 < 0)
                        w[0] = Color.FromArgb(0, 0, 0);
                    else
                        w[0] = bmp_crop.GetPixel(x - 1, y - 1);

                    if (y - 1 < 0)
                        w[1] = Color.FromArgb(0, 0, 0);
                    else
                        w[1] = bmp_crop.GetPixel(x, y - 1);

                    if (x + 1 > bmp_crop.Width - 1 || y - 1 < 0)
                        w[2] = Color.FromArgb(0, 0, 0);
                    else
                        w[2] = bmp_crop.GetPixel(x + 1, y - 1);

                    if (x - 1 < 0)
                        w[3] = Color.FromArgb(0, 0, 0);
                    else
                        w[3] = bmp_crop.GetPixel(x - 1, y);

                    w[4] = bmp_crop.GetPixel(x, y);

                    if (x + 1 > bmp_crop.Width - 1)
                        w[5] = Color.FromArgb(0, 0, 0);
                    else
                        w[5] = bmp_crop.GetPixel(x + 1, y);

                    if (x - 1 < 0 || y + 1 > bmp_crop.Height - 1)
                        w[6] = Color.FromArgb(0, 0, 0);
                    else
                        w[6] = bmp_crop.GetPixel(x - 1, y + 1);

                    if (y + 1 > bmp_crop.Height - 1)
                        w[7] = Color.FromArgb(0, 0, 0);
                    else
                        w[7] = bmp_crop.GetPixel(x, y + 1);

                    if (x + 1 > bmp_crop.Width - 1 || y + 1 > bmp_crop.Height - 1)
                        w[8] = Color.FromArgb(0, 0, 0);
                    else
                        w[8] = bmp_crop.GetPixel(x + 1, y + 1);


                    for (int i = 0; i < w.Length; i++)
                    {
                        xr = xr + (w[i].R * filter_sharpen[i]);
                        xg = xg + (w[i].G * filter_sharpen[i]);
                        xb = xb + (w[i].B * filter_sharpen[i]);
                    }
                    if (xr < 0) xr = -xr;
                    if (xr > 255) xr = 255;

                    if (xg < 0) xg = -xg;
                    if (xg > 255) xg = 255;

                    if (xb < 0) xb = -xb;
                    if (xb > 255) xb = 255;

                    Color new_w = Color.FromArgb(xr, xg, xb);
                    bmp_sharpen.SetPixel(x, y, new_w);
                }
            }
            bmp_crop = bmp_sharpen;
            pictureBox_crop.Image = bmp_crop;
            labeltext.Text = "sharpened";
        }

        private void btn_biner_Click(object sender, EventArgs e)
        {
            bmp_biner = new Bitmap(bmp_crop);
            int white = 255;
            int threshold = Convert.ToInt16(edit_biner.Text);
            // int threshold_kanan = Convert.ToInt16(edit_biner_kanan.Text);
            int middle = bmp_biner.Width / 3 * 2;

            for (int x = 0; x < bmp_crop.Width; x++)
            {
                for (int y = 0; y < bmp_crop.Height; y++)
                {
                    Color w = bmp_crop.GetPixel(x, y);
                    int r = w.R; int g = w.G; int b = w.B;

                    double xr = Math.Sqrt((Math.Pow(r - white, 2)) + (Math.Pow(g - white, 2)) + (Math.Pow(b - white, 2)));
                    int new_xr = 0;


                    if (xr < threshold)
                        new_xr = 255;

                    Color new_w = Color.FromArgb(new_xr, new_xr, new_xr);
                    bmp_biner.SetPixel(x, y, new_w);
                }
            }
            //bmp_crop = bmp_biner;
            pictureBox_biner.Image = bmp_biner;
            labeltext.Text = "hor: " + bmp_crop.Width + "\nver: " + bmp_crop.Height;
            drawHistogram(bmp_biner, white);
            txt_chart.Text = "Chart Putih Bukan Putih";

        }

        private void drawHistogram(Bitmap bmpOrigin, int biner)
        {
            chart_hor.Series["Horizontal"].Points.Clear();
            chart_ver.Series["Vertical"].Points.Clear();

            int[] horizontal = new int[bmpOrigin.Width];
            int[] vertical = new int[bmpOrigin.Height];
            int[] horizontal_difference = new int[bmpOrigin.Width];
            int[] vertical_difference = new int[bmpOrigin.Height];

            dataGrid_hor.Rows.Clear();
            dataGrid_hor.Refresh();
            dataGrid_ver.Rows.Clear();
            dataGrid_ver.Refresh();

            dataGrid_hor.ColumnCount = 3;
            dataGrid_hor.Columns[0].Name = "Index";
            dataGrid_hor.Columns[1].Name = "W Hor";
            dataGrid_hor.Columns[2].Name = "Dif";

            dataGrid_ver.ColumnCount = 3;
            dataGrid_ver.Columns[0].Name = "Index";
            dataGrid_ver.Columns[1].Name = "W Ver";
            dataGrid_ver.Columns[2].Name = "Dif";
            String[] s2 = new String[] { "", "", "" };

            for (int i = 0; i < bmpOrigin.Width; i++)
            {
                horizontal[i] = 0;
            }
            for (int i = 0; i < bmpOrigin.Height; i++)
            {
                vertical[i] = 0;
            }

            for (int x = 0; x < bmpOrigin.Width; x++)
            {
                for (int y = 0; y < bmpOrigin.Height; y++)
                {
                    Color color = bmpOrigin.GetPixel(x, y);
                    if (color.R == biner && color.G == biner && color.B == biner)
                    {
                        horizontal[x]++;
                        //white_sum++; //find rasio
                    }
                    //black_sum++; //find rasio
                }
                chart_hor.Series["Horizontal"].Points.AddXY(x, horizontal[x]);
                if (x == 0)
                {
                    horizontal_difference[x] = 0;
                }
                else
                {
                    horizontal_difference[x] = Math.Abs(horizontal[x - 1] - horizontal[x]);
                }
                s2 = new String[] { Convert.ToString(x), Convert.ToString(horizontal[x]), Convert.ToString(horizontal_difference[x]) };
                dataGrid_hor.Rows.Add(s2);
            }

            for (int y = 0; y < bmpOrigin.Height; y++)
            {
                for (int x = 0; x < bmpOrigin.Width; x++)
                {
                    Color color = bmpOrigin.GetPixel(x, y);
                    if (color.R == biner && color.G == biner && color.B == biner)
                    {
                        vertical[y]++;
                        //s = Convert.ToString("masuk");
                    }
                }
                chart_ver.Series["Vertical"].Points.AddXY(y, vertical[y]);
                if (y == 0)
                {
                    vertical_difference[y] = 0;
                }
                else
                {
                    vertical_difference[y] = Math.Abs(vertical[y - 1] - vertical[y]);
                }
                s2 = new String[] { Convert.ToString(y), Convert.ToString(vertical[y]), Convert.ToString(vertical_difference[y]) };
                dataGrid_ver.Rows.Add(s2);
            }
            findPoint(horizontal, vertical, bmpOrigin);
        }

        private void findPoint(int[] horizontal_difference, int[] vertical_difference, Bitmap bitmapEdited)
        {

            int leftPoint = 0, rightPoint = 0, topPoint = 0, bottomPoint = 0;
            int leftThreshold = 0;
            int rightThreshold = 0;
            int topThreshold = 0;
            int bottomThreshold = 0;
            /* the last 1*/
            /*
            int leftIndexMinimum = bmp_crop.Width / 80 * 16;
              int leftIndexMaximum = bmp_crop.Width / 80 * 26;

              int rightIndexMinimum = bmp_crop.Width / 80 * 65;
              int rightIndexMaximum = bmp_crop.Width / 80 * 55;

              int topIndexMinimum = bmp_crop.Height / 106 * 32;
              int topIndexMaximum = bmp_crop.Height / 106 * 42;

              int bottomIndexMinimum = bmp_crop.Height / 106 * 76; //78
              int bottomIndexMaximum = bmp_crop.Height / 106 * 66; //68
              */
            
            int leftIndexMinimum = bmp_crop.Width / 100 * 22;
            int leftIndexMaximum = bmp_crop.Width / 100 * 39;

            int rightIndexMinimum = bmp_crop.Width / 100 * 88;
            int rightIndexMaximum = bmp_crop.Width / 100 * 70;

            int topIndexMinimum = bmp_crop.Height / 100 * 5;
            int topIndexMaximum = bmp_crop.Height / 100 * 26;

            int bottomIndexMinimum = bmp_crop.Height / 100 * 97; //78
            int bottomIndexMaximum = bmp_crop.Height / 100 * 76; //68
            

            int mid_left_right = bmp_crop.Width / 2;
            int mid_top_bottom = bmp_crop.Height / 2;
            int left = 0; int top = 0;
            int rght = bmp_crop.Width - 1;
            int btm = bmp_crop.Height - 1;
            

            //left
            for (int i = left; i <= leftIndexMaximum; i++)
            {
                if (horizontal_difference[i] > leftThreshold)
                {
                    leftThreshold = horizontal_difference[i];
                    leftPoint = i;
                    //break;
                }
            }


            //right
            for (int i = rght; i >= rightIndexMaximum; i--)
            {
                if (horizontal_difference[i] > rightThreshold)
                {
                    rightThreshold = horizontal_difference[i];
                    rightPoint = i;
                    //break;
                }
            }

            //top
            for (int i = top; i <= topIndexMaximum; i++)
            {
                if (vertical_difference[i] > topThreshold)
                {
                    topThreshold = vertical_difference[i];
                    topPoint = i;
                    //break;
                }
            }

            //bottom
            for (int i = btm; i >= bottomIndexMaximum; i--)
            {
                if (vertical_difference[i] > bottomThreshold)
                {
                    bottomThreshold = vertical_difference[i];
                    bottomPoint = i;
                    //break;
                }
            }

            if (leftThreshold <= 0)
                leftPoint = leftIndexMaximum;
            if (rightThreshold <= 0)
                rightPoint = rightIndexMaximum;
            if (topThreshold <= 0)
                topPoint = topIndexMaximum;
            if (bottomThreshold <= 0)
                bottomPoint = bottomIndexMaximum;

            ed_kiri.Text = Convert.ToString(leftPoint);
            ed_kanan.Text = Convert.ToString(rightPoint);
            ed_atas.Text = Convert.ToString(topPoint);
            ed_bawah.Text = Convert.ToString(bottomPoint);

        }

        private void btn_crop_iris_Click(object sender, EventArgs e)
        {
            int rightPoint = Convert.ToInt16(ed_kanan.Text);
            int leftPoint = Convert.ToInt16(ed_kiri.Text);
            int topPoint = Convert.ToInt16(ed_atas.Text);
            int bottomPoint = Convert.ToInt16(ed_bawah.Text);


            int crop_width = rightPoint - leftPoint;
            int crop_height = bottomPoint - topPoint;

            bmp_crop_color = new Bitmap(crop_width, crop_height);
            bmp_crop_biner = new Bitmap(crop_width, crop_height);

            int xc = 0, yc = 0;
            for (int x = leftPoint + 1; x <= rightPoint; x++)
            {
                yc = 0;
                for (int y = topPoint + 1; y <= bottomPoint; y++)
                {
                    Color w = bmp_crop.GetPixel(x, y);
                    Color w_biner = bmp_biner.GetPixel(x, y);

                    bmp_crop_color.SetPixel(xc, yc, w);
                    bmp_crop_biner.SetPixel(xc, yc, w_biner);
                    yc++;
                }
                xc++;
            }
            pb_crop_iris.Image = bmp_crop_color;
        }

        private void pictureBox_base_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }


        private void btn_jarak_Click(object sender, EventArgs e)
        {
            int black = 0; int white = 255;
            int left = 0, right = 0, top = 0, bottom = 0;
            int mid_width = bmp_crop_biner.Width / 2;
            int mid_height = bmp_crop_biner.Height / 2;

            /*int leftMax = bmp_crop_biner.Width / 3;
            int rightMax = bmp_crop_biner.Width / 3 * 2;

            int topMax = bmp_crop_biner.Height / 3;
            int bottomMax = bmp_crop_biner.Height / 3 * 2;*/

            int leftMax = bmp_crop_biner.Width / 3;
            int rightMax = bmp_crop_biner.Width / 3 * 2;

            int topMax = bmp_crop_biner.Height / 3;
            int bottomMax = bmp_crop_biner.Height / 3 * 2;

            //atas
            for (int y = 0; y <= topMax; y++)
            {
                int x = mid_width;
                Color w = bmp_crop_biner.GetPixel(x, y);
                if (w.R == black || y == topMax)
                {
                    top = y;
                    break;
                }
            }

            //bawah
            for (int y = bmp_crop_biner.Height - 1; y >= bottomMax; y--)
            {
                int x = mid_width;
                Color w = bmp_crop_biner.GetPixel(x, y);
                if (w.R == black || y == bottomMax)
                {
                    bottom = y;
                    break;
                }
            }

            //kiri
            for (int x = 0; x <= leftMax; x++)
            {
                int y = mid_height;
                Color w = bmp_crop_biner.GetPixel(x, y);
                if (w.R == black || x == leftMax)
                {
                    left = x;
                    break;
                }
            }

            //kanan
            //for (int x = mid_width; x <= bmp_crop_biner.Width - 1; x++)
            for (int x = bmp_crop_biner.Width - 1; x >= rightMax; x--)
            {
                int y = mid_height;
                Color w = bmp_crop_biner.GetPixel(x, y);
                //  if (w.R == white || x == bmp_crop_biner.Width - 1)
                if (w.R == black || x == rightMax)
                {
                    right = x;
                    break;
                }

            }

            for (int x = 0; x < bmp_crop_biner.Width; x++)
            {
                for (int y = 0; y < bmp_crop_biner.Height; y++)
                {
                    //kesamping
                    if ((x <= left || x >= right) && y == mid_height)
                    //  if (x <= left && y == mid_height)
                    {
                        bmp_crop_biner.SetPixel(x, y, Color.Red);
                    }
                    /* else if ((x >= mid_width && x <= right) && y == mid_height)
                     {
                         bmp_crop_biner.SetPixel(x, y, Color.Red);
                     }*/
                    //vertikal
                    else if ((y <= top || y >= bottom) && x == mid_width)
                    {
                        bmp_crop_biner.SetPixel(x, y, Color.Red);
                    }
                }
            }

            if (bottom <= top)
                bottom = bmp_crop_biner.Height;
            if (right <= left)
                right = bmp_crop_biner.Width;

            ed_atas2.Text = Convert.ToString(top);
            ed_bawah2.Text = Convert.ToString(bottom);
            ed_kanan2.Text = Convert.ToString(right);
            ed_kiri2.Text = Convert.ToString(left);

            pb_crop_iris.Image = bmp_crop_biner;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void chart_hor_Click(object sender, EventArgs e)
        {

        }

        private void btn_circle_Click(object sender, EventArgs e)
        {
            int rightPoint = Convert.ToInt16(ed_kanan2.Text);
            int leftPoint = Convert.ToInt16(ed_kiri2.Text);
            int topPoint = Convert.ToInt16(ed_atas2.Text);
            int bottomPoint = Convert.ToInt16(ed_bawah2.Text);


            int crop_width = rightPoint - leftPoint;
            int crop_height = bottomPoint - topPoint;
            bmp_crop_color2 = new Bitmap(crop_width, crop_height);

            int xc = 0, yc = 0;
            for (int x = leftPoint + 1; x <= rightPoint; x++)
            {
                yc = 0;
                for (int y = topPoint + 1; y <= bottomPoint; y++)
                {
                    Color w = bmp_crop_color.GetPixel(x, y);

                    bmp_crop_color2.SetPixel(xc, yc, w);
                    yc++;
                }
                xc++;
            }
            pb_biner_crop.Image = bmp_crop_color2;
            bmp_crop_color2.Save("E:\\TA\\DATA\\gambar\\coba\\tes\\Hasil Crop Upd 23 Ags 2021\\Crop.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
        }


        private void btn_check_Click(object sender, EventArgs e)
        {
            Form2 form;
            if (bmp_crop_color2 != null)
                form = new Form2(bmp_crop_color2);
            else
                form = new Form2();
            form.Show();

        }

        private void pictureBox_crop_Click(object sender, EventArgs e)
        {

        }


        private void btn_tes_Click(object sender, EventArgs e)
        {
            Form3 form;

            form = new Form3();
            form.Show();
        }

        /*  private void btnForm3_Click(object sender, EventArgs e)
          {
              Form3 form;

              form = new Form3();
              form.Show();
          }
          */




    }
}
