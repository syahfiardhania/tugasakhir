﻿namespace ImageProcessing
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pictureBox_crop = new System.Windows.Forms.PictureBox();
            this.btn_load = new System.Windows.Forms.Button();
            this.btn_sharp = new System.Windows.Forms.Button();
            this.btn_crop = new System.Windows.Forms.Button();
            this.btn_medianfilter = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox_base = new System.Windows.Forms.PictureBox();
            this.labeltext = new System.Windows.Forms.Label();
            this.btn_biner = new System.Windows.Forms.Button();
            this.pictureBox_biner = new System.Windows.Forms.PictureBox();
            this.edit_biner = new System.Windows.Forms.TextBox();
            this.txt_preprocess = new System.Windows.Forms.Label();
            this.chart_hor = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.txt_chart = new System.Windows.Forms.Label();
            this.chart_ver = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ed_kiri = new System.Windows.Forms.TextBox();
            this.ed_kanan = new System.Windows.Forms.TextBox();
            this.ed_bawah = new System.Windows.Forms.TextBox();
            this.ed_atas = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pb_crop_iris = new System.Windows.Forms.PictureBox();
            this.btn_crop_iris = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGrid_ver = new System.Windows.Forms.DataGridView();
            this.dataGrid_hor = new System.Windows.Forms.DataGridView();
            this.ed_atas2 = new System.Windows.Forms.TextBox();
            this.ed_bawah2 = new System.Windows.Forms.TextBox();
            this.ed_kanan2 = new System.Windows.Forms.TextBox();
            this.ed_kiri2 = new System.Windows.Forms.TextBox();
            this.btn_jarak = new System.Windows.Forms.Button();
            this.jrk_bawah = new System.Windows.Forms.Label();
            this.jrk_atas = new System.Windows.Forms.Label();
            this.jrk_kanan = new System.Windows.Forms.Label();
            this.jrk_kiri = new System.Windows.Forms.Label();
            this.pb_biner_crop = new System.Windows.Forms.PictureBox();
            this.btn_circle = new System.Windows.Forms.Button();
            this.btn_check = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_tes = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_crop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_base)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_biner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_hor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_ver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_crop_iris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_ver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_hor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_biner_crop)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_crop
            // 
            this.pictureBox_crop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_crop.Location = new System.Drawing.Point(16, 426);
            this.pictureBox_crop.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_crop.Name = "pictureBox_crop";
            this.pictureBox_crop.Size = new System.Drawing.Size(381, 178);
            this.pictureBox_crop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_crop.TabIndex = 1;
            this.pictureBox_crop.TabStop = false;
            this.pictureBox_crop.Click += new System.EventHandler(this.pictureBox_crop_Click);
            // 
            // btn_load
            // 
            this.btn_load.Location = new System.Drawing.Point(458, 124);
            this.btn_load.Margin = new System.Windows.Forms.Padding(4);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(100, 116);
            this.btn_load.TabIndex = 2;
            this.btn_load.Text = "Load";
            this.btn_load.UseVisualStyleBackColor = true;
            this.btn_load.Click += new System.EventHandler(this.btn_load_Click);
            // 
            // btn_sharp
            // 
            this.btn_sharp.Location = new System.Drawing.Point(458, 543);
            this.btn_sharp.Margin = new System.Windows.Forms.Padding(4);
            this.btn_sharp.Name = "btn_sharp";
            this.btn_sharp.Size = new System.Drawing.Size(100, 28);
            this.btn_sharp.TabIndex = 3;
            this.btn_sharp.Text = "Sharpen";
            this.btn_sharp.UseVisualStyleBackColor = true;
            this.btn_sharp.Click += new System.EventHandler(this.btn_sharp_Click);
            // 
            // btn_crop
            // 
            this.btn_crop.Location = new System.Drawing.Point(458, 452);
            this.btn_crop.Margin = new System.Windows.Forms.Padding(4);
            this.btn_crop.Name = "btn_crop";
            this.btn_crop.Size = new System.Drawing.Size(100, 28);
            this.btn_crop.TabIndex = 4;
            this.btn_crop.Text = "Crop";
            this.btn_crop.UseVisualStyleBackColor = true;
            this.btn_crop.Click += new System.EventHandler(this.btn_crop_Click);
            // 
            // btn_medianfilter
            // 
            this.btn_medianfilter.Location = new System.Drawing.Point(458, 488);
            this.btn_medianfilter.Margin = new System.Windows.Forms.Padding(4);
            this.btn_medianfilter.Name = "btn_medianfilter";
            this.btn_medianfilter.Size = new System.Drawing.Size(100, 28);
            this.btn_medianfilter.TabIndex = 5;
            this.btn_medianfilter.Text = "Median Filter";
            this.btn_medianfilter.UseVisualStyleBackColor = true;
            this.btn_medianfilter.Click += new System.EventHandler(this.btn_medianfilter_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pictureBox_base
            // 
            this.pictureBox_base.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_base.Location = new System.Drawing.Point(16, 15);
            this.pictureBox_base.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_base.Name = "pictureBox_base";
            this.pictureBox_base.Size = new System.Drawing.Size(381, 353);
            this.pictureBox_base.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_base.TabIndex = 0;
            this.pictureBox_base.TabStop = false;
            this.pictureBox_base.Click += new System.EventHandler(this.pictureBox_base_Click);
            // 
            // labeltext
            // 
            this.labeltext.AutoSize = true;
            this.labeltext.ForeColor = System.Drawing.Color.OrangeRed;
            this.labeltext.Location = new System.Drawing.Point(478, 423);
            this.labeltext.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labeltext.Name = "labeltext";
            this.labeltext.Size = new System.Drawing.Size(51, 17);
            this.labeltext.TabIndex = 7;
            this.labeltext.Text = "not yet";
            // 
            // btn_biner
            // 
            this.btn_biner.Location = new System.Drawing.Point(951, 99);
            this.btn_biner.Margin = new System.Windows.Forms.Padding(4);
            this.btn_biner.Name = "btn_biner";
            this.btn_biner.Size = new System.Drawing.Size(100, 28);
            this.btn_biner.TabIndex = 8;
            this.btn_biner.Text = "Binerization";
            this.btn_biner.UseVisualStyleBackColor = true;
            this.btn_biner.Click += new System.EventHandler(this.btn_biner_Click);
            // 
            // pictureBox_biner
            // 
            this.pictureBox_biner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_biner.Location = new System.Drawing.Point(623, 13);
            this.pictureBox_biner.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_biner.Name = "pictureBox_biner";
            this.pictureBox_biner.Size = new System.Drawing.Size(319, 178);
            this.pictureBox_biner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_biner.TabIndex = 9;
            this.pictureBox_biner.TabStop = false;
            // 
            // edit_biner
            // 
            this.edit_biner.Location = new System.Drawing.Point(951, 53);
            this.edit_biner.Margin = new System.Windows.Forms.Padding(4);
            this.edit_biner.Name = "edit_biner";
            this.edit_biner.Size = new System.Drawing.Size(99, 22);
            this.edit_biner.TabIndex = 26;
            this.edit_biner.Text = "175";
            // 
            // txt_preprocess
            // 
            this.txt_preprocess.AutoSize = true;
            this.txt_preprocess.ForeColor = System.Drawing.Color.Black;
            this.txt_preprocess.Location = new System.Drawing.Point(16, 402);
            this.txt_preprocess.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txt_preprocess.Name = "txt_preprocess";
            this.txt_preprocess.Size = new System.Drawing.Size(99, 17);
            this.txt_preprocess.TabIndex = 35;
            this.txt_preprocess.Text = "Preprocessing";
            // 
            // chart_hor
            // 
            chartArea1.Name = "ChartArea1";
            this.chart_hor.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart_hor.Legends.Add(legend1);
            this.chart_hor.Location = new System.Drawing.Point(623, 221);
            this.chart_hor.Margin = new System.Windows.Forms.Padding(4);
            this.chart_hor.Name = "chart_hor";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Horizontal";
            this.chart_hor.Series.Add(series1);
            this.chart_hor.Size = new System.Drawing.Size(428, 171);
            this.chart_hor.TabIndex = 41;
            this.chart_hor.Text = "chart1";
            this.chart_hor.Click += new System.EventHandler(this.chart_hor_Click);
            // 
            // txt_chart
            // 
            this.txt_chart.AutoSize = true;
            this.txt_chart.ForeColor = System.Drawing.Color.Snow;
            this.txt_chart.Location = new System.Drawing.Point(700, 588);
            this.txt_chart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txt_chart.Name = "txt_chart";
            this.txt_chart.Size = new System.Drawing.Size(42, 17);
            this.txt_chart.TabIndex = 42;
            this.txt_chart.Text = "Chart";
            // 
            // chart_ver
            // 
            chartArea2.Name = "ChartArea1";
            this.chart_ver.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart_ver.Legends.Add(legend2);
            this.chart_ver.Location = new System.Drawing.Point(623, 400);
            this.chart_ver.Margin = new System.Windows.Forms.Padding(4);
            this.chart_ver.Name = "chart_ver";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Vertical";
            this.chart_ver.Series.Add(series2);
            this.chart_ver.Size = new System.Drawing.Size(413, 172);
            this.chart_ver.TabIndex = 43;
            this.chart_ver.Text = "chart1";
            // 
            // ed_kiri
            // 
            this.ed_kiri.Location = new System.Drawing.Point(1790, 60);
            this.ed_kiri.Margin = new System.Windows.Forms.Padding(4);
            this.ed_kiri.Name = "ed_kiri";
            this.ed_kiri.Size = new System.Drawing.Size(99, 22);
            this.ed_kiri.TabIndex = 45;
            this.ed_kiri.Text = "0";
            // 
            // ed_kanan
            // 
            this.ed_kanan.Location = new System.Drawing.Point(1790, 124);
            this.ed_kanan.Margin = new System.Windows.Forms.Padding(4);
            this.ed_kanan.Name = "ed_kanan";
            this.ed_kanan.Size = new System.Drawing.Size(99, 22);
            this.ed_kanan.TabIndex = 46;
            this.ed_kanan.Text = "0";
            // 
            // ed_bawah
            // 
            this.ed_bawah.Location = new System.Drawing.Point(1790, 233);
            this.ed_bawah.Margin = new System.Windows.Forms.Padding(4);
            this.ed_bawah.Name = "ed_bawah";
            this.ed_bawah.Size = new System.Drawing.Size(99, 22);
            this.ed_bawah.TabIndex = 47;
            this.ed_bawah.Text = "0";
            // 
            // ed_atas
            // 
            this.ed_atas.Location = new System.Drawing.Point(1790, 175);
            this.ed_atas.Margin = new System.Windows.Forms.Padding(4);
            this.ed_atas.Name = "ed_atas";
            this.ed_atas.Size = new System.Drawing.Size(99, 22);
            this.ed_atas.TabIndex = 48;
            this.ed_atas.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.OrangeRed;
            this.label1.Location = new System.Drawing.Point(1821, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 17);
            this.label1.TabIndex = 49;
            this.label1.Text = "Kiri";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.OrangeRed;
            this.label2.Location = new System.Drawing.Point(1822, 101);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 50;
            this.label2.Text = "Kanan";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.OrangeRed;
            this.label3.Location = new System.Drawing.Point(1822, 210);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 51;
            this.label3.Text = "Bawah";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.OrangeRed;
            this.label4.Location = new System.Drawing.Point(1821, 153);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 17);
            this.label4.TabIndex = 52;
            this.label4.Text = "Atas";
            // 
            // pb_crop_iris
            // 
            this.pb_crop_iris.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_crop_iris.Location = new System.Drawing.Point(1564, 44);
            this.pb_crop_iris.Margin = new System.Windows.Forms.Padding(4);
            this.pb_crop_iris.Name = "pb_crop_iris";
            this.pb_crop_iris.Size = new System.Drawing.Size(193, 178);
            this.pb_crop_iris.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_crop_iris.TabIndex = 53;
            this.pb_crop_iris.TabStop = false;
            // 
            // btn_crop_iris
            // 
            this.btn_crop_iris.Location = new System.Drawing.Point(1562, 230);
            this.btn_crop_iris.Margin = new System.Windows.Forms.Padding(4);
            this.btn_crop_iris.Name = "btn_crop_iris";
            this.btn_crop_iris.Size = new System.Drawing.Size(89, 33);
            this.btn_crop_iris.TabIndex = 54;
            this.btn_crop_iris.Text = "Crop 1";
            this.btn_crop_iris.UseVisualStyleBackColor = true;
            this.btn_crop_iris.Click += new System.EventHandler(this.btn_crop_iris_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Snow;
            this.label6.Location = new System.Drawing.Point(1085, 320);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 17);
            this.label6.TabIndex = 73;
            this.label6.Text = "Vertikal";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Snow;
            this.label5.Location = new System.Drawing.Point(1085, 15);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 17);
            this.label5.TabIndex = 72;
            this.label5.Text = "Horizontal";
            // 
            // dataGrid_ver
            // 
            this.dataGrid_ver.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGrid_ver.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_ver.Location = new System.Drawing.Point(1088, 362);
            this.dataGrid_ver.Margin = new System.Windows.Forms.Padding(4);
            this.dataGrid_ver.Name = "dataGrid_ver";
            this.dataGrid_ver.RowHeadersWidth = 51;
            this.dataGrid_ver.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGrid_ver.Size = new System.Drawing.Size(395, 210);
            this.dataGrid_ver.TabIndex = 71;
            // 
            // dataGrid_hor
            // 
            this.dataGrid_hor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGrid_hor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_hor.Location = new System.Drawing.Point(1089, 44);
            this.dataGrid_hor.Margin = new System.Windows.Forms.Padding(4);
            this.dataGrid_hor.Name = "dataGrid_hor";
            this.dataGrid_hor.RowHeadersWidth = 51;
            this.dataGrid_hor.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGrid_hor.Size = new System.Drawing.Size(395, 210);
            this.dataGrid_hor.TabIndex = 70;
            // 
            // ed_atas2
            // 
            this.ed_atas2.Location = new System.Drawing.Point(1792, 426);
            this.ed_atas2.Margin = new System.Windows.Forms.Padding(4);
            this.ed_atas2.Name = "ed_atas2";
            this.ed_atas2.Size = new System.Drawing.Size(99, 22);
            this.ed_atas2.TabIndex = 91;
            this.ed_atas2.Text = "0";
            // 
            // ed_bawah2
            // 
            this.ed_bawah2.Location = new System.Drawing.Point(1792, 490);
            this.ed_bawah2.Margin = new System.Windows.Forms.Padding(4);
            this.ed_bawah2.Name = "ed_bawah2";
            this.ed_bawah2.Size = new System.Drawing.Size(99, 22);
            this.ed_bawah2.TabIndex = 90;
            this.ed_bawah2.Text = "0";
            // 
            // ed_kanan2
            // 
            this.ed_kanan2.Location = new System.Drawing.Point(1792, 370);
            this.ed_kanan2.Margin = new System.Windows.Forms.Padding(4);
            this.ed_kanan2.Name = "ed_kanan2";
            this.ed_kanan2.Size = new System.Drawing.Size(99, 22);
            this.ed_kanan2.TabIndex = 89;
            this.ed_kanan2.Text = "0";
            // 
            // ed_kiri2
            // 
            this.ed_kiri2.Location = new System.Drawing.Point(1792, 306);
            this.ed_kiri2.Margin = new System.Windows.Forms.Padding(4);
            this.ed_kiri2.Name = "ed_kiri2";
            this.ed_kiri2.Size = new System.Drawing.Size(99, 22);
            this.ed_kiri2.TabIndex = 88;
            this.ed_kiri2.Text = "0";
            // 
            // btn_jarak
            // 
            this.btn_jarak.Location = new System.Drawing.Point(1666, 230);
            this.btn_jarak.Margin = new System.Windows.Forms.Padding(4);
            this.btn_jarak.Name = "btn_jarak";
            this.btn_jarak.Size = new System.Drawing.Size(92, 33);
            this.btn_jarak.TabIndex = 87;
            this.btn_jarak.Text = "Proses";
            this.btn_jarak.UseVisualStyleBackColor = true;
            this.btn_jarak.Click += new System.EventHandler(this.btn_jarak_Click);
            // 
            // jrk_bawah
            // 
            this.jrk_bawah.AutoSize = true;
            this.jrk_bawah.ForeColor = System.Drawing.Color.OrangeRed;
            this.jrk_bawah.Location = new System.Drawing.Point(1822, 470);
            this.jrk_bawah.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.jrk_bawah.Name = "jrk_bawah";
            this.jrk_bawah.Size = new System.Drawing.Size(50, 17);
            this.jrk_bawah.TabIndex = 86;
            this.jrk_bawah.Text = "Bawah";
            // 
            // jrk_atas
            // 
            this.jrk_atas.AutoSize = true;
            this.jrk_atas.ForeColor = System.Drawing.Color.OrangeRed;
            this.jrk_atas.Location = new System.Drawing.Point(1822, 406);
            this.jrk_atas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.jrk_atas.Name = "jrk_atas";
            this.jrk_atas.Size = new System.Drawing.Size(36, 17);
            this.jrk_atas.TabIndex = 85;
            this.jrk_atas.Text = "Atas";
            // 
            // jrk_kanan
            // 
            this.jrk_kanan.AutoSize = true;
            this.jrk_kanan.ForeColor = System.Drawing.Color.OrangeRed;
            this.jrk_kanan.Location = new System.Drawing.Point(1817, 351);
            this.jrk_kanan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.jrk_kanan.Name = "jrk_kanan";
            this.jrk_kanan.Size = new System.Drawing.Size(49, 17);
            this.jrk_kanan.TabIndex = 84;
            this.jrk_kanan.Text = "Kanan";
            // 
            // jrk_kiri
            // 
            this.jrk_kiri.AutoSize = true;
            this.jrk_kiri.ForeColor = System.Drawing.Color.OrangeRed;
            this.jrk_kiri.Location = new System.Drawing.Point(1817, 288);
            this.jrk_kiri.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.jrk_kiri.Name = "jrk_kiri";
            this.jrk_kiri.Size = new System.Drawing.Size(28, 17);
            this.jrk_kiri.TabIndex = 83;
            this.jrk_kiri.Text = "Kiri";
            // 
            // pb_biner_crop
            // 
            this.pb_biner_crop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_biner_crop.Location = new System.Drawing.Point(1565, 294);
            this.pb_biner_crop.Margin = new System.Windows.Forms.Padding(4);
            this.pb_biner_crop.Name = "pb_biner_crop";
            this.pb_biner_crop.Size = new System.Drawing.Size(193, 178);
            this.pb_biner_crop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_biner_crop.TabIndex = 82;
            this.pb_biner_crop.TabStop = false;
            // 
            // btn_circle
            // 
            this.btn_circle.Location = new System.Drawing.Point(1604, 494);
            this.btn_circle.Margin = new System.Windows.Forms.Padding(4);
            this.btn_circle.Name = "btn_circle";
            this.btn_circle.Size = new System.Drawing.Size(117, 33);
            this.btn_circle.TabIndex = 81;
            this.btn_circle.Text = "Crop 2";
            this.btn_circle.UseVisualStyleBackColor = true;
            this.btn_circle.Click += new System.EventHandler(this.btn_circle_Click);
            // 
            // btn_check
            // 
            this.btn_check.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_check.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_check.Location = new System.Drawing.Point(1562, 588);
            this.btn_check.Margin = new System.Windows.Forms.Padding(4);
            this.btn_check.Name = "btn_check";
            this.btn_check.Size = new System.Drawing.Size(267, 82);
            this.btn_check.TabIndex = 92;
            this.btn_check.Text = "Check Form 2";
            this.btn_check.UseVisualStyleBackColor = false;
            this.btn_check.Click += new System.EventHandler(this.btn_check_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(499, 317);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 17);
            this.label7.TabIndex = 93;
            this.label7.Text = "dhani";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // btn_tes
            // 
            this.btn_tes.Location = new System.Drawing.Point(233, 904);
            this.btn_tes.Name = "btn_tes";
            this.btn_tes.Size = new System.Drawing.Size(75, 23);
            this.btn_tes.TabIndex = 94;
            this.btn_tes.Text = "button1";
            this.btn_tes.UseVisualStyleBackColor = true;
            this.btn_tes.Click += new System.EventHandler(this.btn_tes_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkKhaki;
            this.ClientSize = new System.Drawing.Size(1924, 1055);
            this.Controls.Add(this.btn_tes);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_check);
            this.Controls.Add(this.ed_atas2);
            this.Controls.Add(this.ed_bawah2);
            this.Controls.Add(this.ed_kanan2);
            this.Controls.Add(this.ed_kiri2);
            this.Controls.Add(this.btn_jarak);
            this.Controls.Add(this.jrk_bawah);
            this.Controls.Add(this.jrk_atas);
            this.Controls.Add(this.jrk_kanan);
            this.Controls.Add(this.jrk_kiri);
            this.Controls.Add(this.pb_biner_crop);
            this.Controls.Add(this.btn_circle);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dataGrid_ver);
            this.Controls.Add(this.dataGrid_hor);
            this.Controls.Add(this.btn_crop_iris);
            this.Controls.Add(this.pb_crop_iris);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ed_atas);
            this.Controls.Add(this.ed_bawah);
            this.Controls.Add(this.ed_kanan);
            this.Controls.Add(this.ed_kiri);
            this.Controls.Add(this.chart_ver);
            this.Controls.Add(this.txt_chart);
            this.Controls.Add(this.chart_hor);
            this.Controls.Add(this.txt_preprocess);
            this.Controls.Add(this.edit_biner);
            this.Controls.Add(this.pictureBox_biner);
            this.Controls.Add(this.btn_biner);
            this.Controls.Add(this.labeltext);
            this.Controls.Add(this.btn_medianfilter);
            this.Controls.Add(this.btn_crop);
            this.Controls.Add(this.btn_sharp);
            this.Controls.Add(this.btn_load);
            this.Controls.Add(this.pictureBox_crop);
            this.Controls.Add(this.pictureBox_base);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Iridology";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_crop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_base)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_biner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_hor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_ver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_crop_iris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_ver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_hor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_biner_crop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_crop;
        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.Button btn_sharp;
        private System.Windows.Forms.Button btn_crop;
        private System.Windows.Forms.Button btn_medianfilter;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox_base;
        private System.Windows.Forms.Label labeltext;
        private System.Windows.Forms.Button btn_biner;
        private System.Windows.Forms.PictureBox pictureBox_biner;
        private System.Windows.Forms.TextBox edit_biner;
        private System.Windows.Forms.Label txt_preprocess;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_hor;
        private System.Windows.Forms.Label txt_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_ver;
        private System.Windows.Forms.TextBox ed_kiri;
        private System.Windows.Forms.TextBox ed_kanan;
        private System.Windows.Forms.TextBox ed_bawah;
        private System.Windows.Forms.TextBox ed_atas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pb_crop_iris;
        private System.Windows.Forms.Button btn_crop_iris;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGrid_ver;
        private System.Windows.Forms.DataGridView dataGrid_hor;
        private System.Windows.Forms.TextBox ed_atas2;
        private System.Windows.Forms.TextBox ed_bawah2;
        private System.Windows.Forms.TextBox ed_kanan2;
        private System.Windows.Forms.TextBox ed_kiri2;
        private System.Windows.Forms.Button btn_jarak;
        private System.Windows.Forms.Label jrk_bawah;
        private System.Windows.Forms.Label jrk_atas;
        private System.Windows.Forms.Label jrk_kanan;
        private System.Windows.Forms.Label jrk_kiri;
        private System.Windows.Forms.PictureBox pb_biner_crop;
        private System.Windows.Forms.Button btn_circle;
        private System.Windows.Forms.Button btn_check;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_tes;
        //private System.Windows.Forms.Button btnForm3;
    }
}

