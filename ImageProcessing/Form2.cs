﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageProcessing
{
    public partial class Form2 : Form
    {
        Bitmap bmp_base, bmp_roi, bmp_canny, bmp_temp, bmp_roi2;
        double r_black = 0, r_white = 0;
        double r_black2 = 0, r_white2 = 0;
        int[,] matrix = new int[3, 3];
        int[,] sobelX = new int[3, 3];
        int[,] sobelY = new int[3, 3];

        public Form2()
        {
            InitializeComponent();
            Inisialisasi();
        }


        public Form2(Bitmap bmp_crop_color2)
        {
            InitializeComponent();
            Inisialisasi();
            Bitmap bmp = bmp_crop_color2;
            bmp_base = new Bitmap(bmp, new Size(300, 300));

            pb_crop_iris.Image = bmp_base;
        }

        public void Inisialisasi()
        {
            sobelX[0, 0] = -3; sobelX[0, 1] = 0; sobelX[0, 2] = 3;
            sobelX[1, 0] = -10; sobelX[1, 1] = 0; sobelX[1, 2] = 10;
            sobelX[2, 0] = -3; sobelX[2, 1] = 0; sobelX[2, 2] = 3;
            sobelY[0, 0] = -3; sobelY[0, 1] = -10; sobelY[0, 2] = -3;
            sobelY[1, 0] = 0; sobelY[1, 1] = 0; sobelY[1, 2] = 0;
            sobelY[2, 0] = 3; sobelY[2, 1] = 10; sobelY[2, 2] = 3;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                Bitmap bmp = new Bitmap(openFileDialog1.FileName);
                bmp_base = new Bitmap(bmp, new Size(300, 300)); //lebar tinggi
                pb_crop_iris.Image = bmp_base;
            }
        }

        private void btn_sobel_Click(object sender, EventArgs e)
        {
            Bitmap bmp_sobel;
            
            //convert to Gray
            bmp_sobel = bmp_base;
            for (int y = 0; y < bmp_sobel.Height; y++)
            {
                for (int x = 0; x < bmp_sobel.Width; x++)
                {
                    Color w = bmp_sobel.GetPixel(x, y);
                    int gray = (int)(w.R + w.G + w.B) / 3;
                    bmp_sobel.SetPixel(x, y, Color.FromArgb(gray, gray, gray));
                }
            }
            int count = 1;
            Bitmap bmp_sobel_x;
            //konvolusi filter_x
            bmp_sobel_x = new Bitmap(bmp_sobel);
            for (int y = 1; y < bmp_sobel.Height - 1; y++)
            {
                for (int x = 1; x < bmp_sobel.Width - 1; x++)
                {
                    int hasil = getValueKonvolusi(sobelX, bmp_sobel, x, y);
                    bmp_sobel_x.SetPixel(x, y, Color.FromArgb(hasil, hasil, hasil));
                }
            }

            Bitmap bmp_sobel_y;
            //konvolusi filter_y
            bmp_sobel_y = new Bitmap(bmp_sobel);
            for (int y = 1; y < bmp_sobel.Height - 1; y++)
            {
                for (int x = 1; x < bmp_sobel.Width - 1; x++)
                {
                    int hasil = getValueKonvolusi(sobelY, bmp_sobel, x, y);
                    bmp_sobel_y.SetPixel(x, y, Color.FromArgb(hasil, hasil, hasil));
                }
            }

            //sobel
            Bitmap bmp_sobel2a = bmp_sobel_x, bmp_sobel2b = bmp_sobel_y;
            Bitmap bmp_sobel_fix = new Bitmap(bmp_sobel);
            
            for (int y = 1; y < bmp_sobel.Height - 1; y++)
            {
                for (int x = 1; x < bmp_sobel.Width - 1; x++)
                {
                    int hasil = getMagnitude(bmp_sobel2a, bmp_sobel2b, x, y);
                    
                    bmp_sobel_fix.SetPixel(x, y, Color.FromArgb(hasil, hasil, hasil));
                    
                }
            }


            bmp_temp = bmp_base;
            bmp_base = bmp_sobel_fix;
            pb_sobel.Image = bmp_base;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txt_bw2_Click(object sender, EventArgs e)
        {

        }


        private void pb_extract_Click(object sender, EventArgs e)
        {

        }

        public int getValueKonvolusi(int[,] filter, Bitmap bmp1, int x, int y)
        {
            int value = 0;
            value += filter[0, 0] * bmp1.GetPixel(x - 1, y - 1).R;
            value += filter[0, 1] * bmp1.GetPixel(x, y - 1).R;
            value += filter[0, 2] * bmp1.GetPixel(x + 1, y - 1).R;
            value += filter[1, 0] * bmp1.GetPixel(x - 1, y).R;
            value += filter[1, 1] * bmp1.GetPixel(x, y).R;
            value += filter[1, 2] * bmp1.GetPixel(x + 1, y).R;
            value += filter[2, 0] * bmp1.GetPixel(x - 1, y + 1).R;
            value += filter[2, 1] * bmp1.GetPixel(x, y + 1).R;
            value += filter[2, 2] * bmp1.GetPixel(x + 1, y + 1).R;
            if (value < 0) value = 0;
            if (value > 255) value = 255;
            return value;
        }

        public int getMagnitude(Bitmap bmp2a, Bitmap bmp2b, int x, int y)
        {
            int value1 = bmp2a.GetPixel(x, y).R;
            int value2 = bmp2a.GetPixel(x, y).R;
            int hasil = (int)Math.Sqrt((Math.Pow(value1, 2)) + Math.Pow(value2, 2));
            if (hasil > 255)
                hasil = 255;
            return hasil;
        }

        private void btn_roi_Click(object sender, EventArgs e)
        {
            //Penyakit Diabetes
            //Area Kanan
            /*int left = bmp_base.Width * 11 / 32;
            int right = bmp_base.Width * 13 / 32;
            int top = Convert.ToInt16(bmp_base.Height * 21 / 32);
            int bottom = Convert.ToInt16(bmp_base.Height * 23.5 / 32);
            */
            //Area Kiri
            int left = bmp_base.Width * 20 / 32;
            int right = bmp_base.Width * 23 / 32;
            int top = Convert.ToInt16(bmp_base.Height * 19 / 32);
            int bottom = Convert.ToInt16(bmp_base.Height * 21 / 32);

            //Penyakit Jantung
            //7, 9.5, 4, 8.5
            //Area KANAN
            /*int left2 = Convert.ToInt16(bmp_base.Width * 8 / 32);
            int right2 = Convert.ToInt16(bmp_base.Width * 10 / 32);
            int top2 = Convert.ToInt16(bmp_base.Height * 12 / 32);
            int bottom2 = Convert.ToInt16(bmp_base.Height * 16 / 32);
            */
            //Area KIRI
            int left2 = Convert.ToInt16(bmp_base.Width * 22 / 32);
            int right2 = Convert.ToInt16(bmp_base.Width * 25 / 32);
            int top2 = Convert.ToInt16(bmp_base.Height * 12 / 32);
            int bottom2 = Convert.ToInt16(bmp_base.Height * 17 / 32);

            Bitmap roi_area = new Bitmap(bmp_base);
            for (int i = 0; i < bmp_base.Width; i++)
            {
                for (int j = 0; j < bmp_base.Height; j++)
                {
                    //kebawah
                    if ((i == left || i == right) && j >= top && j <= bottom)
                    {
                        roi_area.SetPixel(i, j, Color.Red);
                    }
                    //kesamping
                    else if (i >= left && i <= right && (j == top || j == bottom))
                    {
                        roi_area.SetPixel(i, j, Color.Red);
                    }
                }
            }

            Bitmap roi_area2 = new Bitmap(bmp_base);
            for (int i = 0; i < bmp_base.Width; i++)
            {
                for (int j = 0; j < bmp_base.Height; j++)
                {
                    //kebawah
                    if ((i == left2 || i == right2) && j >= top2 && j <= bottom2)
                    {
                        roi_area2.SetPixel(i, j, Color.Yellow);
                    }
                    //kesamping
                    else if (i >= left2 && i <= right2 && (j == top2 || j == bottom2))
                    {
                        roi_area2.SetPixel(i, j, Color.Yellow);
                    }
                }
            }

            pb_roi.Image = roi_area;
            pb_roi2.Image = roi_area2;
            //pb_roi.Image = roi_area2;

            bmp_roi = new Bitmap(right - left + 1, bottom - top + 1);
            int xb = 0, yb = 0;

            for (int x = left; x <= right; x++)
            {
                for (int y = top; y <= bottom; y++)
                {
                    Color color = bmp_base.GetPixel(x, y);
                    bmp_roi.SetPixel(xb, yb, color);
                    yb++;
                }
                xb++;
                yb = 0;
            }

            //baru
            bmp_roi2 = new Bitmap(right2 - left2 + 1, bottom2 - top2 + 1);
            int xb2 = 0, yb2 = 0;

            //baru
            for (int x = left2; x <= right2; x++)
            {
                for (int y = top2; y <= bottom2; y++)
                {
                    Color color = bmp_base.GetPixel(x, y);
                    bmp_roi2.SetPixel(xb2, yb2, color);
                    yb2++;
                }
                xb2++;
                yb2 = 0;
            }

            pb_roi_img.Image = bmp_roi;
            pb_roi_img2.Image = bmp_roi2;
        }

        private void btn_rasio_Click(object sender, EventArgs e)
        {
            //Penyakit Diabetes
            Bitmap bmp_roi_extract = new Bitmap(bmp_roi);
            Bitmap bmp_roi_gray = new Bitmap(bmp_roi);
            int black = 0, white = 255;
            double black_count = 0, white_count = 0;
            double count = 0; int lain = 0;

            //Penyakit Jantung
            Bitmap bmp_roi_extract2 = new Bitmap(bmp_roi2);
            Bitmap bmp_roi_gray2 = new Bitmap(bmp_roi2);
            int black2 = 0, white2 = 255;
            double black2_count = 0, white2_count = 0;
            double count2 = 0; int lain2 = 0;


            
            /*

            double blackGrayscale = 0, whiteGrayscale = 0;
            for (int i = 0; i < bmp_roi_gray.Width; i++)
            {
                for (int j = 0; j < bmp_roi_gray.Height; j++)
                {
                    Color color = bmp_roi.GetPixel(i, j);
                    int cGray = (color.R + color.G + color.B) / 3;

                    int cBlackWhite;
                    if (cGray <= 50)
                    {
                        blackGrayscale++;
                        cBlackWhite = 0;
                    }
                    else
                    {
                        whiteGrayscale++;
                        cBlackWhite = 255;
                    }

                    Color newColor = Color.FromArgb(cBlackWhite, cBlackWhite, cBlackWhite);
                    bmp_roi_gray.SetPixel(i, j, newColor);
                }
            }*/
            ///////////////////////////////////////////////////////////////
            
            //Penyakit Diabetes
            int new_xr = 0;
            for (int x = 0; x < bmp_roi.Width; x++)
            {
                for (int y = 0; y < bmp_roi.Height; y++)
                {
                    Color w = bmp_roi.GetPixel(x, y);
                    double xr = Math.Sqrt((Math.Pow(w.R - white, 2)) + (Math.Pow(w.G - white, 2)) + (Math.Pow(w.B - white, 2)));

                    if (xr < 350)
                    {
                        new_xr = 255;
                        white_count++;
                    }
                    else
                    {
                        new_xr = 0;
                        black_count++;
                    }
                    count++;
                    Color new_w = Color.FromArgb(new_xr, new_xr, new_xr);
                    //bmp_roi_extract.SetPixel(x, y, new_w);
                    bmp_roi_extract.SetPixel(x, y, new_w);
                }
            }

            //Penyakit Jantung
            int new_xr2 = 0;
            for (int x = 0; x < bmp_roi2.Width; x++)
            {
                for (int y = 0; y < bmp_roi2.Height; y++)
                {
                    Color w2 = bmp_roi2.GetPixel(x, y);
                    double xr2 = Math.Sqrt((Math.Pow(w2.R - white2, 2)) + (Math.Pow(w2.G - white2, 2)) + (Math.Pow(w2.B - white2, 2)));
                    if (xr2 < 350)
                    {
                        new_xr2 = 255;
                        white2_count++;
                    }
                    else
                    {
                        new_xr2 = 0;
                        black2_count++;
                    }
                    count2++;
                    Color new_w2 = Color.FromArgb(new_xr2, new_xr2, new_xr2);
                    bmp_roi_extract2.SetPixel(x, y, new_w2);
                }
            }

            //bmp_roi = bmp_roi_extract;

            //Penyakit Diabetes
            pb_extract.Image = bmp_roi_extract;
            txt_bw.Text = "Jumlah hitam = " + Convert.ToInt16(black_count) +
                "\nJumlah putih = " + Convert.ToInt16(white_count) +
                "\nJumlah lain = " + Convert.ToInt16(lain) +
                "\nJumlah pixel = " + Convert.ToInt16(count);
           

            r_black = black_count / count;
            r_white = white_count / count;

            txt_ratio_bw.Text = "Ratio hitam = " + Math.Round(r_black, 3) +
               "\nRatio putih = " + Math.Round(r_white, 3);

            //Penyakit Jantung
            pb_extract2.Image = bmp_roi_extract2;
            txt_bw2.Text = "Jumlah hitam = " + Convert.ToInt16(black2_count) +
                "\nJumlah putih = " + Convert.ToInt16(white2_count) +
                "\nJumlah lain = " + Convert.ToInt16(lain2) +
                "\nJumlah pixel = " + Convert.ToInt16(count2);

            r_black2 = black2_count / count2;
            r_white2 = white2_count / count2;

            txt_ratio_bw2.Text = "Ratio hitam = " + Math.Round(r_black2, 3) +
                "\nRatio putih = " + Math.Round(r_white2, 3);
        
        }

        private void btn_kondisi_Click(object sender, EventArgs e)
        {
            double th_white = Convert.ToDouble(edit_threshold.Text);
            //double th_white = 0.25;
            if (th_white < r_white)
                tx_hasil.Text = "HASIL: Abnormal (Teridetifikasi Dibetes)";           
            else           
                tx_hasil.Text = "HASIL: Normal (Tidak Teridentifikasi Penyakit Diabetes)";
                                 
        }

        private void btn_kondisi2_Click(object sender, EventArgs e)
        {
            double th_white2 = Convert.ToDouble(edit_threshold2.Text);
            //double th_white2 = 0.25;
            if (th_white2 < r_white2)
                tx_hasil2.Text = "\n HASIL: Abnormal (Teridentifikasi Jantung)";
            else
                tx_hasil2.Text = "\n HASIL: Normal (Tidak Teridentifikasi Penyakit Jantung)";
        }


        ///////////////////////////////grayscale//////////////////////////////////////

        /*
        pictureBox_grayScaleBiner.Image = bmp_roi_gray;
        blackGrayscale = blackGrayscale / count;
        whiteGrayscale = whiteGrayscale / count;
        txt_ratio_gs.Text = "Ratio hitam = " + Math.Round(blackGrayscale, 3) +
           "\nRatio putih = " + Math.Round(whiteGrayscale, 3);*/


    }
}
